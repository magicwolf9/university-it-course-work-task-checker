﻿using DBBot.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace DBBot
{
    public static class WebApiConfig
    {
        public static TelegramBotSettingsSection _Config =
            ConfigurationManager.GetSection("telegramBotSettings") as TelegramBotSettingsSection;


        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "TgApi",
                routeTemplate: String.Format(_Config.webhookRoute, _Config.secureToken),
                defaults: new { controller = "TelegramWebhook" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
