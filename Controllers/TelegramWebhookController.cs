﻿using DBBot.Configuration;
using DBBot.Models;
using DBBot.Models.Commands;
using DBBot.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace DBBot.Controllers
{
    public class TelegramWebhookController : ApiController
    {
        public static TelegramBotSettingsSection _Config =
            ConfigurationManager.GetSection("telegramBotSettings") as TelegramBotSettingsSection;

        private DatabaseService databaseService = new DatabaseService();
        private HometaskService hometaskService = new HometaskService();

        [HttpPost]
        public async Task<OkResult> Post([FromBody]Update update)
        {
            if (update == null) return Ok();

            var commands = Bot.Commands;
            var message = update.Message;
            var botClient = await Bot.GetBotClientAsync();

            bool commandFound = false;
            foreach (var command in commands)
            {
                if (command.Contains(message))
                {
                    commandFound = true;
                    await command.Execute(message, botClient);
                    break;
                }
            }

            if(!commandFound)
            {
                try
                {
                    UserState userState = databaseService.GetUserState(message.Chat.Id);
                    if (userState != null && userState.stateId == 1)
                    {
                        HometaskResult result;
                        try
                        {
                            result = hometaskService.CheckHometask(message.Text);
                        } catch (Exception e)
                        {
                            string userMessage = 
                                @"Ошибка при проверке задания. Вводите процедуры в подобном формате: 
                                CREATE PROCEDURE DoSomething
                                AS
                                BEGIN
                                    — тело процедуры
                                END";
                            await botClient.SendTextMessageAsync(message.Chat.Id, userMessage, parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                            return Ok();
                        }

                        try
                        {
                            databaseService.InsertAttempt(message.Chat.Id, message.Text, result.score);
                        }
                        catch (Exception e)
                        {
                            // nothing
                        }
                        await botClient.SendTextMessageAsync(message.Chat.Id, $"Вы набрали: {result.score}. Детали: {result.details}", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                    }
                } catch (Exception e)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка6 ${e.Message}", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                }
            }

            return Ok();
        }
    }
}
