﻿using DBBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBBot.Controllers
{
    public class HomeController : Controller
    {
        private HometaskService hometaskService = new HometaskService();
        public string hometaskValue { get; set; }

        public ActionResult Index()
        {
            int[] a;

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string HometaskValue)
        {

            string message = "Вы набрали: ";
            HometaskResult result;
            try
            {
                result = hometaskService.CheckHometask(HometaskValue);
            }
            catch (Exception e)
            {
                string userMessage =
                    @"Ошибка при проверке задания. Вводите процедуры в подобном формате:<br/> 
                                CREATE PROCEDURE DoSomething<br/>
                                AS<br/>
                                BEGIN<br/>
                                    — тело процедуры<br/>
                                END<br/>";

                ViewBag.details = userMessage;
                return View();
            }
            ViewBag.result = message + result.score;
            ViewBag.details = result.details.Replace("\n", "<br/>");
            return View();
        }
    }
}