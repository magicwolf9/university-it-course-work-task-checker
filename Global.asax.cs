using DBBot.Configuration;
using DBBot.Models.Commands;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Telegram.Bot;

namespace DBBot
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static TelegramBotSettingsSection _Config =
            ConfigurationManager.GetSection("telegramBotSettings") as TelegramBotSettingsSection;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Bot.GetBotClientAsync().Wait();
        }
    }
}
