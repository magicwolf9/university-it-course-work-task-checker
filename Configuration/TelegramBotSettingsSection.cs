﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DBBot.Configuration
{
    public class TelegramBotSettingsSection: ConfigurationSection
    {
        [ConfigurationProperty("secureToken", IsRequired = true)]
        public string secureToken
        {
            get { return (string)this["secureToken"]; }
            set { this["secureToken"] = value; }
        }

        [ConfigurationProperty("webhookRoute", IsRequired = true)]
        public string webhookRoute
        {
            get { return (string)this["webhookRoute"]; }
            set { this["webhookRoute"] = value; }
        }

        [ConfigurationProperty("dbConn", IsRequired = true)]
        public string dbConn
        {
            get { return (string)this["dbConn"]; }
            set { this["dbConn"] = value; }
        }
    }
}