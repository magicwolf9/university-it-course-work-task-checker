﻿using DBBot.Configuration;
using DBBot.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DBBot.Services
{

    public class DatabaseService
    {
        public static TelegramBotSettingsSection _Config =
            ConfigurationManager.GetSection("telegramBotSettings") as TelegramBotSettingsSection;

        public void InsertAttempt(long userId, string attemptData, double result)
        {
            using (SqlConnection connection = new SqlConnection(_Config.dbConn))
            {
                try
                {
                    SqlCommand command = new SqlCommand("INSERT INTO dbo.hometask_attempt (user_id, attempt_data, result) VALUES (@UserId,@AttemptData,@Result)", connection);

                    command.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = userId.ToString();
                    command.Parameters.Add("@AttemptData", SqlDbType.NVarChar).Value = attemptData;
                    command.Parameters.Add("@Result", SqlDbType.NVarChar).Value = result.ToString();

                    connection.Open();
                    command.ExecuteScalar();
                    connection.Close();
                } finally
                {
                    connection.Close();
                }
            }
        }

        public UserState GetUserState(long userId)
        {
            using (SqlConnection connection = new SqlConnection(_Config.dbConn))
            {
                try
                {
                    SqlCommand command = new SqlCommand("SELECT id, user_id, state_id, prev_state_id, value" +
                        " FROM dbo.user_state WHERE user_id = @UserId", connection);

                    command.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = userId.ToString();
                    
                    connection.Open();
                    SqlDataReader dr = command.ExecuteReader();
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            UserState state = new UserState();
                            state.id = dr.GetInt64(0);
                            state.userId = dr.GetString(1);
                            state.stateId = dr.GetInt64(2);
                            state.prevState = dr.IsDBNull(3) ? -1 : dr.GetInt64(3);
                            state.value = dr.IsDBNull(4) ? null : dr.GetString(4);
                            connection.Close();
                            return state;
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            return null;
        }

        public void InsertUserState(long userId, int stateId, string prevStateId = null, string value = null)
        {
            using (SqlConnection connection = new SqlConnection(_Config.dbConn))
            {
                try
                {
                    SqlCommand command = new SqlCommand("INSERT INTO dbo.user_state (user_id, state_id, prev_state_id, value) VALUES (@UserId, @StateId, @PrevStateId, @Value)", connection);

                    command.Parameters.Add("@UserId", SqlDbType.VarChar).Value = userId;
                    command.Parameters.Add("@StateId", SqlDbType.Int).Value = stateId;
                    if (prevStateId == null)
                    {
                        command.Parameters.Add("@PrevStateId", SqlDbType.BigInt).Value = DBNull.Value;
                    }
                    else
                    {
                        command.Parameters.Add("@PrevStateId", SqlDbType.BigInt).Value = prevStateId;
                    }
                    if (value == null)
                    {
                        command.Parameters.Add("@Value", SqlDbType.NVarChar).Value = DBNull.Value;
                    }
                    else
                    {
                        command.Parameters.Add("@Value", SqlDbType.NVarChar).Value = value;
                    }
                    connection.Open();
                    command.ExecuteScalar();
                    connection.Close();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

    }
}