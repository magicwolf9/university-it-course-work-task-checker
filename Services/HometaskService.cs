﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace DBBot.Services
{
    public class HometaskResult
    {
        public double score;
        public string details;
    }

    public class HometaskService
    {
        public HometaskResult CheckHometask(string rawData)
        {
            string formattedInput = this.PreFormat(rawData);

            HometaskResult result = new HometaskResult();
            result.score= 0;
            result.details = "";

            foreach (string procedure in formattedInput
                .Split(new string[] { "create " }, StringSplitOptions.None)
                .Where(item => item != "")
                )
            {
                string mainBlock = this.CleanText(this.RemoveTextSQLValues(this.ExtractLogicalBlock(procedure)));

                double score = this.ProcessBlock(mainBlock);
                result.details += $"\n\n\n\n--- START BLOCK---\nGOT {score} FOR {procedure}\n\n\n\n---END BLOCK---";

                result.score += score;
            }

            return result;
        }

        private double ProcessBlock(string text = "")
        {
            double points = 0;

            points += Regex.Matches(text, @"insert").Count * 0.5;
            points += Regex.Matches(text, @"delete").Count * 0.5;
            points += Regex.Matches(text, @"update").Count * 0.5;

            if (text.IndexOf("whereand") != -1 || text.IndexOf("whereor") != -1)
            {
                points += 0.2;
            }
            if (text.IndexOf("groupbyhaving") != -1)
            {
                points += 0.3;
            }
            else if (text.IndexOf("groupby") != -1)
            {
                points += 0.2;
            }

            if (Regex.Matches(text, @"selectjoin?[\w\(\)]+?join?[\w\(\)]+?join?[\w\(\)]+?(where|groupby|;)").Count != 0)
            {
                points += 0.3;
            } else if (Regex.Matches(text, @"selectjoin?[\w\(\)]+?join?[\w\(\)]+?(where|groupby|;)").Count != 0) {

                points += 0.2;
            }

            if (text.IndexOf("select(select") != -1 || text.IndexOf("selectwhere(select") != -1)
            {
                points += 0.2;
            }

            return points;
        }

        private string ExtractLogicalBlock(string text)
        {
            int startInd = text.IndexOf("begin");
            int endInd = text.LastIndexOf("end");
            int substrLength = endInd - (startInd + "begin".Length);
            return startInd != -1 && endInd != -1 ?
                text.Substring(startInd + "begin".Length, substrLength) : null;
        }

        private string RemoveTextSQLValues(string text)
        {
            int commaCount = 0;
            string resultString = "";
            char[] textAsCharArray = text.ToCharArray();

            for (int i = 0; i < textAsCharArray.Length; i++)
            {

                char currentChar = textAsCharArray[i];
                if (currentChar == '\'' && textAsCharArray[i - 1] != '\\')
                {
                    commaCount += 1;
                }
                else if (commaCount % 2 == 0)
                {
                    resultString += currentChar;
                }
            }

            return resultString;
        }

        private string CleanText(string text)
        {
            MatchCollection matches = Regex.Matches(text.ToLower(), @"select\s|insert\s|update\s|delete\s|;|\(|\)|where\s|and\s|or\s|join\s|group\sby\s|having\s");

            string cleanedText = "";
            foreach (Match match in matches)
            {
                cleanedText += Regex.Replace(match.Value, @"\s", ""); ;
            }

            return Regex.Replace(cleanedText, @"\(\)", "");
        }

        private string PreFormat(string text)
        {
            string replacedText = text.ToLower().Replace("create or replace", "create");
            return replacedText.Substring(replacedText.IndexOf("create"));
        }



    }
}