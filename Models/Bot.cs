﻿using DBBot.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot;

namespace DBBot.Models.Commands
{
    public class Bot
    {
        private static TelegramBotClient botClient;
        private static List<Command> commandsList;
        public static TelegramBotSettingsSection _Config =
            ConfigurationManager.GetSection("telegramBotSettings") as TelegramBotSettingsSection;

        public static IReadOnlyList<Command> Commands => commandsList.AsReadOnly();

        public static async Task<TelegramBotClient> GetBotClientAsync()
        {
            if (botClient != null)
            {
                return botClient;
            }

            commandsList = new List<Command>();
            commandsList.Add(new StartCommand());
            commandsList.Add(new CheckTaskCommand());
            //TODO: Add more commands

            botClient = new TelegramBotClient(_Config.secureToken);
            string hook = string.Format("https://dbbot20191228032417.azurewebsites.net/" + _Config.webhookRoute, _Config.secureToken);
            await botClient.SetWebhookAsync(hook);
            return botClient;
        }
    }
}