﻿using DBBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace DBBot.Models.Commands
{
    public class CheckTaskCommand: Command
    { 
        public override string Name => @"/check";
        private DatabaseService databaseService = new DatabaseService();

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            var chatId = message.Chat.Id;
            try
            {
                databaseService.InsertUserState(message.Chat.Id, 1, null, null);
            } catch (Exception e)
            {
                await botClient.SendTextMessageAsync(chatId, $"Ошибка6 ${e.Message}", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                return;
            }
            await botClient.SendTextMessageAsync(chatId, $"Отправьте код процедуры или функции на проверку", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
    }
}