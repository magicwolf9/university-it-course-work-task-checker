﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace DBBot.Models.Commands
{
    public class StartCommand: Command
    { 
        public override string Name => @"/start";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            
            var chatId = message.Chat.Id;
            await botClient.SendTextMessageAsync(chatId, "Hallo I'm MISIS hometask checker bot. Наберите /check чтобы начать проверку задания лабораторной работы по БД", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
    }
}