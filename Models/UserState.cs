﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBBot.Models
{
    public class UserState
    {
        public long id;
        public string userId;
        public long stateId;
        public long prevState;
        public string value;
    }
}